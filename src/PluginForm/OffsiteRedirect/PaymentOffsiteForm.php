<?php

namespace Drupal\commerce_payment_ipay\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class PaymentOffsiteForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $redirect_method = $payment_gateway_plugin->getConfiguration()['redirect_method'];
    $merchant_code = $payment_gateway_plugin->getConfiguration()['merchant_code'];
    $merchant_key = $payment_gateway_plugin->getConfiguration()['merchant_key'];
    $remove_js = ($redirect_method == 'post_manual');
    if (in_array($redirect_method, ['post', 'post_manual'])) {
      $redirect_url = 'https://payment.ipay88.com.my/epayment/entry.asp';
      $responce_url = Url::fromRoute('commerce_payment_ipay.responce', [], ['absolute' => TRUE])->toString();
      $responce_backend_url = Url::fromRoute('commerce_payment_ipay.responce_backend', [], ['absolute' => TRUE])->toString();
      $redirect_method = 'post';
    }   
    $order = $payment->getOrder();
    $billing_profile = $order->getBillingProfile();
    $address_info = $billing_profile->get('address')->getValue();
    $payment_amount = $payment->getAmount()->getNumber();
    $amount_source = number_format($payment_amount, 2);
    $amount_source = str_replace('.', '', $amount_source);
    $signature_source = $merchant_key.$merchant_code.'ORD'.$order->id().$amount_source.'MYR';
    $signature = hash('sha256', $signature_source);
    $data = [
      'return' => $redirect_url,
      'cancel' => $form['#cancel_url'],
      'MerchantCode' => $merchant_code,
      'PaymentId' => 2,
      'RefNo' => 'ORD' . $order->id(),
      'Amount' => number_format($payment_amount, 2),
      'Currency' => 'MYR',
      'ProdDesc' => 'JiozStudio Product',
      'UserName' => $address_info[0]['given_name'] . ' ' .$address_info[0]['family_name'],
      'UserEmail' => $order->getEmail(),
      'UserContact' => $billing_profile->get('field_telepone_no')->getValue()[0]['value'],
      'SignatureType' => 'SHA256',
      'Signature' => $signature,
      'ResponseURL' => $responce_url,
      'BackendURL' => $responce_backend_url,
    ];
    $form = $this->buildRedirectForm($form, $form_state, $redirect_url, $data, $redirect_method);
    if ($remove_js) {
      // Disable the javascript that auto-clicks the Submit button.
      unset($form['#attached']['library']);
    }

    return $form;
  }

}
