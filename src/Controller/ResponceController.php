<?php

namespace Drupal\commerce_payment_ipay\Controller;

//use Drupal\Core\DependencyInjection\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;


/**
 * This is a dummy controller for mocking an off-site gateway.
 */
class ResponceController extends ControllerBase {

  public function responce() {
  	if (!empty($_REQUEST)) {
  		$merchantcode = $_REQUEST["MerchantCode"];
	    $paymentid = $_REQUEST["PaymentId"];
	    $refno = $_REQUEST["RefNo"];
	    $amount = $_REQUEST["Amount"];
	    $ecurrency = $_REQUEST["Currency"];
	    $remark = $_REQUEST["Remark"];
	    $transid = $_REQUEST["TransId"];
	    $authcode = $_REQUEST["AuthCode"];
	    $status = $_REQUEST["Status"];
	    $errdesc = $_REQUEST["ErrDesc"];
	    $signature = $_REQUEST["Signature"];
	    if ($status == 1 && $errdesc == ""){
	    	$oderId = substr($refno, 3);
	    	$path = URL::fromUserInput('/checkout/' . $oderId . '/payment/return', ['query' => $_REQUEST])->toString();
	    	return new RedirectResponse($path);
	    }
	    else {
	    	\Drupal::logger('Response ipay88')->notice("<pre>" .print_r($_REQUEST, true). "</pre>");
	    	$oderId = substr($refno, 3);
	    	$path = URL::fromUserInput('/checkout/' . $oderId . '/payment/cancel', ['query' => $_REQUEST])->toString();
	    	return new RedirectResponse($path);
	    }
  	}
  }

  public function responce_backend(){
    $build = [
      '#type' => 'markup',
      '#markup' => $this->t('RECEIVEOK')
    ];
    return $build;
  }

}
